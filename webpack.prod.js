const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

module.exports = {
	// watch: true,
	watchOptions: {
		aggregateTimeout: 200,
		poll: 200,
		ignored: /node_modules/
	},
	entry: './src/js/index.js',
	mode: 'production', // development - production
	output: {
		filename: '[hash].js',
		path: path.resolve(__dirname, 'build')
	},
	plugins: [
		/* new FaviconsWebpackPlugin({
			logo: './src/img/logo_mymetromobilit.png',
			mode: 'webapp',
			devMode: 'webapp',
			cache: true,
			publicPath: '/favicons',
			outputPath: '/img/favicons',
			prefix: 'dist/',
			nject: true,
			favicons: {
				appName: 'My Metromobilité',
				appDescription: 'My Metromobilité - Transports de l\'agglomération grenobloise. Horaires, info trafic.',
				developerName: 'Baptiste Brand',
				developerURL: null, // prevent retrieving from the nearest package.json
				background: '#1b1e23',
				theme_color: '#333',
				icons: {
				  coast: false,
				  yandex: false
				}
			}
		}), */
		new CleanWebpackPlugin({
			verbose: false,
			cleanStaleWebpackAssets: false,
			protectWebpackAssets: false,
			cleanOnceBeforeBuildPatterns: [
				'**/*', // clean all
				'!views','!views/**/*', // ignore views folder
				'!lang','!lang/**/*',  // ignore lang folder
			],
			cleanAfterEveryBuildPatterns: [
				'fonts/vendor' // remove fonts/vendor folder after build and watch
			],
		}),
		new webpack.LoaderOptionsPlugin({
			options: {
				postcss: [
					autoprefixer()
				]
			}
		}),
		new MiniCssExtractPlugin({
			filename: '[hash].css',
			chunkFilename: '[id].css'
		}),
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, 'src/index.html'),
			filename: 'index.html',
			cache: false,
			inject: true,
			minify: {
				collapseWhitespace: true,
				removeComments: true,
				removeRedundantAttributes: true,
				removeScriptTypeAttributes: true,
				removeStyleLinkTypeAttributes: true,
				useShortDoctype: true
			}
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		  }),
	],

	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
				  loader: 'babel-loader',
				  options: {
					presets: ['@babel/preset-env'],
					plugins: ['@babel/plugin-proposal-object-rest-spread']
				  }
				}
			  },
			{
				test: /\.html$/,
				loader: 'html-loader',
			},
			{
				test: /\.s[ac]ss$/i,
				exclude: /node_modules/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'postcss-loader',
					'sass-loader'
				]
			},
			{
				test: /\.(ico|jpg|jpeg|png|gif|webp|svg)(\?.*)?$/,
				// exclude: /node_modules/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[hash].[ext]',
						outputPath: 'img/',
						publicPath: 'img/'
					},
				}, 
			},
			{
				test: /\.(eot|otf|ttf|woff|woff2)(\?.*)?$/,
				// exclude: /node_modules/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[hash].[ext]',
						outputPath: 'fonts/'
					},
				},
			},
		]
	}
};
