const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

module.exports = {
	// watch: true,
	devServer: {
		contentBase: path.join(__dirname, 'build'),
		compress: true,
		port: 9000
	},
	watchOptions: {
		aggregateTimeout: 200,
		poll: 200,
		ignored: /node_modules/
	},
	entry: './src/js/index.js',
	mode: 'development', // development - production
	output: {
		filename: '[name].js',
        path: path.resolve(__dirname, 'build')
	},
	plugins: [
		new CleanWebpackPlugin({
            verbose: false,
            cleanStaleWebpackAssets: false,
            protectWebpackAssets: false,
            cleanOnceBeforeBuildPatterns: [
                '**/*', // clean all
                '!views','!views/**/*', // ignore views folder
                '!lang','!lang/**/*',  // ignore lang folder
            ],
            cleanAfterEveryBuildPatterns: [
                'fonts/vendor' // remove fonts/vendor folder after build and watch
            ],
		}),
		new webpack.LoaderOptionsPlugin({
			options: {
				postcss: [
					autoprefixer()
				]
			}
		}),
		new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
          }),
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, 'src/index.html'),
			filename: 'index.html',
			cache: false,
			inject: true,
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		  }),
	],

	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
				  loader: 'babel-loader',
				  options: {
					presets: ['@babel/preset-env'],
					plugins: ['@babel/plugin-proposal-object-rest-spread']
				  }
				}
			  },
			{
				test: /\.html$/,
				loader: 'html-loader',
			},
			{
                test: /\.s[ac]ss$/i,
                exclude: /node_modules/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'postcss-loader',
					'sass-loader'
				]
			},
			{
                test: /\.(ico|jpg|jpeg|png|gif|webp|svg)(\?.*)?$/,
				// exclude: /node_modules/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'img/',
						publicPath: 'img/'
					},
				}, 
			},
			{
                test: /\.(eot|otf|ttf|woff|woff2)(\?.*)?$/,
                // exclude: /node_modules/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'fonts/'
					},
				},
			},
		]
	}
};
