import '../scss/mymetromobilit.scss';
import * as $ from '../../node_modules/jquery';
import L from 'leaflet';
import {changeMapStyle} from './map.js';
import {click, drawMarker} from './utils.js';
import './notify.js';

if (L.Browser.ielt9) alert('GROS! Désinstalle Internet Explorer et change de navigateur car sinon tu peux pas utiliser ce site!');

$( '#changesens' ).on( 'click', function() {
	$.notify("Changement de sens", "success");
	let sens = $(this).attr('data-sens') == 0 ? 1 : 0;

	let p = {
		text: $(this).attr('text'),
		color: $(this).attr('color'),
		background: $(this).attr('background'),
		'data-type': $(this).attr('data-type'),
		title: $(this).attr('title'),
		'data-sens': sens
	};
	drawMarker(p, sens);
});


$(document).on('click', '.transport-content', function(e){
	if(! click )return;
	$('#map-load').addClass('map-load-active');

	e.preventDefault();

	let createElm = true;

	let child = $('#history-elm-content').children();
	if(child.length >= 1){
		for (let i = 0; i < child.length; i++) {
			const element = child[i];
			if($(element).data('type') == $(this).data('type')) createElm = false; //$('#map-load').removeClass('map-load-active');
		}
	}

	let badge;
	if(createElm){
		badge = document.createElement('div');
		$(badge).text($(this).text());
		$(badge).css('color', $(this).css('color'));
		$(badge).css('background', $(this).css('background-color'));
		$(badge).addClass('badge');
		$(badge).attr('data-type', $(this).data('type'));
		$(badge).attr('title', $(this).attr('title'));
		$('.history-elm-content').append(badge);
	}
	// console.log($(this).data('type'))

	let p = {
		text: $(this).attr('text'),
		color: $(this).css('color'),
		background: $(this).css('background-color'),
		'data-type': $(this).attr('data-type'),
		title: $(this).attr('title'),
		'data-sens': $(this).attr('data-sens')
	};

	// console.log(p)
	drawMarker(p, $(this).attr('data-sens'), badge); //$(this), 0, badge
});

$(document).on('click', '.badge', function() {
	if(!click)return;
	$('#map-load').addClass('map-load-active');

	let p = {
		text: $(this).attr('text'),
		color: $(this).css('color'),
		background: $(this).css('background-color'),
		'data-type': $(this).attr('data-type'),
		title: $(this).attr('title'),
		'data-sens': $(this).attr('data-sens')
	};


	drawMarker(p);
});


$('#btntoggle').click(function(){
	$('#transport-container').removeClass('transport-container-collapse');
	$('#line-information-container').removeClass('line-information-container-show');
});


$('#thememode').click( () => changeTheme())

const changeTheme = () => {
	if($('html').attr('data-theme') == 'light'){
		$('html').attr('data-theme', 'dark');
		localStorage.setItem('metrotheme', 'dark')
	} else if($('html').attr('data-theme') == 'dark'){
		localStorage.setItem('metrotheme', 'light')
		$('html').attr('data-theme', 'light');
	}

	changeMapStyle($('html').attr('data-theme'));
	$('#thememode').text($('html').attr('data-theme'))
}

$('#favorites').click(function(){
	let info = $(this).attr('data-type');
	let metrofavorties = JSON.parse(localStorage.getItem('metrofavorties'));

	if($('#iconfavorites').hasClass('iconfavorites-active')){
		metrofavorties.splice(metrofavorties.indexOf( $(this).attr('data-type') ), 1);
		localStorage.setItem('metrofavorties', JSON.stringify(metrofavorties));
		$('#iconfavorites').removeClass('iconfavorites-active');

	} else {
		$('#iconfavorites').addClass('iconfavorites-active');
		if(metrofavorties){
			if(metrofavorties.includes(info)) return;
			metrofavorties.push(info);
			localStorage.setItem('metrofavorties', JSON.stringify(metrofavorties));
		} else {
			let br = [];
			br.push(info);
			localStorage.setItem('metrofavorties', JSON.stringify(br));
		}
	}
});

