const mymap = L.map('mapid').setView([45.18, 5.72], 13);
import * as $ from '../../node_modules/jquery';
import * as L from 'leaflet';
import markerdora from '../img/marker-dora.png';
import markershadow from '../../node_modules/leaflet/dist/images/marker-shadow.png';

let mapstyle = {
	dark: 'mapbox/dark-v10',
	light: 'mapbox/outdoors-v11'
};

const mapboxAttribution = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
const token = 'pk.eyJ1IjoiYXpkcmEiLCJhIjoiY2tlYm55ZDJrMGFtZzJ5cGdyZHQ5bHMzYSJ9.2pX-ARehzZ_YKoe1PO0RpA';

let currentMap;
const changeMapStyle = (id) => {
	if(currentMap) currentMap.remove();
	currentMap = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
		id: mapstyle[id], 
		attribution: mapboxAttribution,
		maxZoom: 18,
		tileSize: 512,
		zoomOffset: -1,
		accessToken: token,
	}).addTo(mymap);
};

$( document ).ready(function() {
	let theme = localStorage.getItem('metrotheme');
	if(theme) changeMapStyle(theme), $('html').attr('data-theme', theme), $('#thememode').text(theme);
	else changeMapStyle($('html').attr('data-theme')), $('#thememode').text($('html').attr('data-theme'));
	
});

const markerDora = L.icon({
	iconUrl: markerdora,
	shadowUrl: markershadow,
	iconSize:     [50, 82],
	shadowSize:   [62, 51],
	iconAnchor:   [10, 90],
	shadowAnchor: [4, 62],
	popupAnchor:  [-3, -76]
});

export {mymap, markerDora, changeMapStyle};