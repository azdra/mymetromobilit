import axios from '../../node_modules/axios';
import $ from '../../node_modules/jquery';
import * as L from 'leaflet';

import {mymap, markerDora} from './map.js';
import { ignorePatterns } from '../../.eslintrc';
let markerGroup;

let click = true;
let tval = false; 

axios.get('https://data.metromobilite.fr/api/routers/default/index/routes')
	.then(res => {
		const resp = res.data;
		const trans = $('.transport-elm-container');

		for (let i = 0; i < resp.length; i++) {
			const ir = resp[i];

			for (let k = 0; k < trans.length; k++) {
				const ek = trans[k];
				const ekem = ek.getAttribute('value');

				if(ekem == ir.type){
					let el = document.createElement('div');
					$(el).text(ir.shortName);
					$(el).css('color', '#'+ir.textColor);
					$(el).css('background', '#'+ir.color);
					$(el).addClass(ekem.toLowerCase()).addClass('transport-content');
					$(el).attr('data-type', ir.id);
					$(el).attr('title', ir.longName);

					$(`#${ir.type}`).append(el);
				}
			} 
		}
	})
	.catch( err => {
		console.error(err);
	})
	.then( () => {
		// click = true;
		// console.warn('Succes');
	});

const drawMarker = (target, meaning, badge) => {
	
	click = false;

	if(!meaning) meaning = 0;

	let time = Date.now();

	if(markerGroup) markerGroup.clearLayers();

	markerGroup = L.layerGroup().addTo(mymap);
	axios.get(`https://data.metromobilite.fr/api/ficheHoraires/json?route=${target['data-type']}&time=${time}`)
		// badge.remove() FIX BADGE IS NOT DEFINED
		.then(res =>  {
			if(res.data[0].arrets.length == 0) {
				$.notify('Aucune information sur cette arret', 'error');
				badge.remove();
			}
			lineInformation(res, meaning, target);
		} )
		.catch( err => console.error(err) )
		.then( () => {
			click = true;
			if(!tval){

				if($('#map-load').hasClass('map-load-active')){
					$('#map-load').removeClass('map-load-active');
				}

				target['data-sens'] = meaning;

				$('#changesens').attr(target);
			
				$('#transport-container').addClass('transport-container-collapse');
				$('#line-information-container').addClass('line-information-container-show');

			// console.warn('Succes');
			}
		}); 
};

const drawLine = (target) => {

	// console.log(target)
	click = false;

	axios.get(`https://data.metromobilite.fr/api/lines/json?types=ligne&codes=${target['data-type']}`)
		.then(res => {
			const resp = res.data;
			

			let coordinates = resp.features[0].geometry.coordinates[0];

			var myLines = [{
				'type': 'LineString',
				'coordinates': coordinates
			}];

			var myStyle = {
				'color': target['background'],
				'weight': 5,
				'opacity': 1
			};
	
			L.geoJSON(myLines, {
				style: myStyle
			}).addTo(markerGroup);
		})
		.catch( err => {
			console.error(err);
		})
		.then( () => {
			click = true;
			// console.warn('Succes');

			if($('#map-load').hasClass('map-load-active')){
				$('#map-load').removeClass('map-load-active');
			}
		});
};

const lineInformation = (res, meaning, target) => {
	const resp = res.data;

	let arrets = {0: resp[meaning].arrets, 1: resp[meaning].arrets};
	if(!arrets[meaning].length) return (
		tval = true,

		$('#map-load').removeClass('map-load-active'),
		$('.direction-name').text('').css('visibility', 'collapse')
	);
	tval = false;

	$('#transtype').addClass('badge');

	$('#favorites').attr('data-type', target['data-type']);

	let metrofavorties = JSON.parse(localStorage.getItem('metrofavorties'));

	if(metrofavorties){
		if(metrofavorties.includes(target['data-type'])){
			$('#iconfavorites').addClass('iconfavorites-active');

		} else {
			$('#iconfavorites').removeClass('iconfavorites-active');

		}
	}

	if(target['data-sens'] == 1){
		let sp = target['title'].split('/');
		// console.log(sp)
		$('.transname > .name').text(`${sp[1]} / ${sp[0]}`);
		$('.direction-name').text(`${sp[1]} / ${sp[0]}`).css('visibility', 'visible');

	} else {
		$('.transname > .name').text(target['title']);
		$('.direction-name').text(target['title']).css('visibility', 'visible');
	}

	for (let i = 0; i < arrets[meaning].length; i++) {
		let r = arrets[meaning][i];
		let p = r.parentStation;
		
		let marker = L.marker([p.lat, p.lon], {
			icon: markerDora,
			draggable: false
		}).addTo(markerGroup);
		marker.bindPopup(`<b>${p.city}</b><br>${p.name}`);
	}
	
	drawLine(target);

	if($('#map-load').hasClass('map-load-active')){
		$('#map-load').removeClass('map-load-active');
	}

	mymap.setView([arrets[meaning][0].lat, arrets[meaning][0].lon],13); 

	$('#transinfo-elm').text('');
	for (let i = 0; i < arrets[meaning].length; i++) {
		const element = arrets[meaning][i];

		let tr = document.createElement('tr');
		$(tr).addClass('station');
		let left = document.createElement('td');
		let right = document.createElement('td');

		$(left).attr('title', element.stopName);
		$(left).addClass('tleft').addClass('w-50').addClass('text-left');
		$(left).text(element.stopName);

		$(right).addClass('tright').addClass('w-50').addClass('text-right');
		$(right).addClass('tright').addClass('w-50').addClass('text-right');

		let unix = eval((element.trips[0]*1000));
		let unixDate = new Date(unix);
		let hours = (unixDate.getHours()+1) < 10 ? '0'+unixDate.getHours() : unixDate.getHours();
		let minutes = unixDate.getMinutes() < 10 ? '0'+unixDate.getMinutes() : unixDate.getMinutes(); 

		$(right).append( `${hours}:${minutes}` );

		tr.append(left, right);
		$('#transinfo-elm').append(tr);
	}
};

$(document).on('click', '.modal-close', function(){
	$('.popus-container').toggleClass('popus-container-collapse');
	setTimeout(() => {
		$('.popus-container').toggleClass('popus-container-collapse');
	}, 5*60*1000);
});

export {click, drawMarker};

