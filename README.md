# MyMetromobilité

You must create a responsive website whose objective is to be able to consult the timetables of all the public transport lines in the Grenoble area.

## Technologies used:
<ul>
    <li>HTML</li>
    <li>SCSS</li>
    <li>jQuery</li>
    <li>Webpack</li>
    <li>ESLint</li>
    <li>PostCSS</li>
    <li>Leaflet</li>
    <li>Axios</li>
</ul>

## Installation
```
npm install
npm run build:prod
```

## Dev Dependencies
<code>npm i -D @babel/core @babel/preset-env autoprefixer babel-loader clean-webpack-plugin css-loader eslint eslint-loader file-loader html-loader html-webpack-plugin image-webpack-loader mini-css-extract-plugin node-sass postcss-cli postcss-loader sass-loader style-loader stylelint stylelint-scss webpack webpack-cli webpack-dev-server</code>

## Dependencies
<code>npm i axios jquery leaflet</code>

## Preview 
![img.png](img.png)
![img_1.png](img_1.png)
