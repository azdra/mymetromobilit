module.exports = {
	'env': {
		'browser': true,
		'es2020': true
	},
	"ignorePatterns": [ 'node_modules/*', 'postcss.config.js', '.eslintrc.js', 'webpack.config.js', 'webpack.prod.js', 'build/*' ],

	'extends': 'eslint:recommended',
	'parserOptions': {
		'ecmaVersion': 11,
		'sourceType': 'module'
	},
	'rules': {
		'indent': [
			'error',
			'tab'
		],
		'linebreak-style': [
			'error',
			'windows'
		],
		'quotes': [
			'error',
			'single'
		],
		'semi': [
			'error',
			'always'
		]
	}
};